﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Entity.Entity
{
    public class Company : EntityBase
    {
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public ICollection<Department> Departments { get; set; }
    }
}
