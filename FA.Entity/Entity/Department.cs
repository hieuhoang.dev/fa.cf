﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Entity.Entity
{
    public class Department : EntityBase
    {
        public string DepartmentCode { get; set; }  
        public string DepartmentName { get; set; }
        public int CompanyId { get; set; }
        public Company Company { get; set; }
    }
}
