﻿using FA.Entity.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Entity.Config
{
    public class DepartmentConfig : IEntityTypeConfiguration<Department>
    {
        public void Configure(EntityTypeBuilder<Department> builder)
        {
            builder.ToTable("Department");
            builder.HasKey(t => t.Id);
            builder.Property(t => t.Id).ValueGeneratedOnAdd();
            builder.HasOne(t => t.Company)
                .WithMany(t => t.Departments)
                .HasForeignKey(t => t.CompanyId);
            builder.Property(t => t.DepartmentCode).IsRequired().HasMaxLength(20);
            builder.Property(t => t.DepartmentName).IsRequired().HasMaxLength(50);
            builder.Property(t => t.IsDelete).HasDefaultValue(false);
            builder.Property(t => t.IsActive).HasDefaultValue(true);
            builder.Property(t => t.CreatedDate).HasDefaultValue(DateTime.Now);
            builder.Property(t => t.UpdatedDate).HasDefaultValue(DateTime.Now);
        }
    }
}
