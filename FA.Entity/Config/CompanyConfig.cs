﻿using FA.Entity.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Entity.Config
{
    public class CompanyConfig : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.ToTable("Company");
            builder.HasKey(t => t.Id);
            builder.Property(t => t.Id).ValueGeneratedOnAdd();
            builder.Property(t => t.CompanyCode).IsRequired().HasMaxLength(20);
            builder.Property(t => t.CompanyName).IsRequired().HasMaxLength(50);
            builder.Property(t => t.IsDelete).HasDefaultValue(false);
            builder.Property(t => t.IsActive).HasDefaultValue(true);
            builder.Property(t => t.CreatedDate).HasDefaultValue(DateTime.Now);
            builder.Property(t => t.UpdatedDate).HasDefaultValue(DateTime.Now);
        }
    }
}
