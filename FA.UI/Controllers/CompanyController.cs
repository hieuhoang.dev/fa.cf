﻿using AutoMapper;
using FA.Entity.Entity;
using FA.Model;
using FA.Model.CompanyModel;
using FA.Repository.UnitOfWork;
using Microsoft.AspNetCore.Mvc;

namespace FA.UI.Controllers
{
    public class CompanyController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CompanyController(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [Route("danh-sach-cong-ty")]
        public IActionResult Companies()
        {
            return View();
        }

        [Route("them-moi-cong-ty")]
        public IActionResult CreateCompany()
        {
            return View();
        }

        [HttpPost]
        public IActionResult InsertCompany(CreateCompanyModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork._companyRepository.Add(_mapper.Map<Company>(model));
                    return Ok();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
           return View(model);
        }

        public IActionResult UpdateCompany(int Id)
        {
            return View();
        }
    }
}
