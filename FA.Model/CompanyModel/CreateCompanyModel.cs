﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Model.CompanyModel
{
    public class CreateCompanyModel
    {
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
    }
}
