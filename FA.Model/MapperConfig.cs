﻿using AutoMapper;
using FA.Entity.Entity;
using FA.Model.CompanyModel;

namespace FA.Model
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<Company, CreateCompanyModel>().ReverseMap();
        }
    }
}
