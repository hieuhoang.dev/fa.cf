﻿using FA.Entity.Entity;
using FA.Repository.GenericRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Repository.DepartmentRepo
{
    public interface IDepartmentRepository : IGenericRepository<Department>
    {
    }
}
