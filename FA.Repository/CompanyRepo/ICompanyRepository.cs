﻿using FA.Entity.Entity;
using FA.Repository.GenericRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Repository.CompanyRepo
{
    public interface ICompanyRepository : IGenericRepository<Company>
    {
    }
}
