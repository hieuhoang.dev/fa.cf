﻿using FA.Entity.Context;
using FA.Entity.Entity;
using FA.Repository.GenericRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FA.Repository.CompanyRepo
{
    public class CompanyRepository : GenericRepository<Company>, ICompanyRepository
    {
        public CompanyRepository(DatabaseContext context) : base(context)
        {
        }
        
    }
}
