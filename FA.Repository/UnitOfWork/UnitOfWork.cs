﻿using FA.Entity.Context;
using FA.Repository.CompanyRepo;
using FA.Repository.DepartmentRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Repository.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DatabaseContext _context;
        public ICompanyRepository _companyRepository => _companyRepository ?? (new CompanyRepository(_context));

        public IDepartmentRepository _departmentRepository => _departmentRepository ?? (new DepartmentRepository(_context));

        public UnitOfWork()
        {
            _context = new DatabaseContext();
        }
        public void Dispose()
        {
            _context.Dispose();
        }

        public void SaveChange()
        {
            _context.SaveChanges();
        }
    }
}
