﻿using FA.Entity.Context;
using FA.Repository.CompanyRepo;
using FA.Repository.DepartmentRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.Repository.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {

        ICompanyRepository _companyRepository { get; }
        IDepartmentRepository _departmentRepository { get; }
        void SaveChange();

    }
}
